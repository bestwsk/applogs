/**
     * @param {Object} options 
     * @param {string} options.self_system
     * @param {string} options.self_group
     * @param {string} options.self_type
     * @param {number|string|null} options.start_time
     * 
     * @param {Object} requestOptions
     * @param {string} requestOptions.uri
     * @param {Object} requestOptions.headers
     * @param {string|null} requestOptions.headers.Authorization
     * @param {number|null} requestOptions.timeout
     * @param {boolean} requestOptions.json
     * @param {string} requestOptions.method
     * 
     * 
     * @param {string} mode - normal, loadtest
     */

const { ApplogModel } = require('./applog');
const moment = require('moment-timezone');
module.exports.createMiddleware = function (requestOptions, options, mode = 'normal') {
  let Log = class extends ApplogModel {
    constructor() {
      super(requestOptions, options, mode)
    }
  }
  return (req, res, next) => {
    req.startEpochTime = getEpochTime();
    const originalSendMethod = res.send;
    res.send = function (data) {
      const log = new Log();
      originalSendMethod.call(this, data);

      /**
      * @param {string|null} message
      * @param {number} severity
      * @param {string|null} selfFunction 
      * @param {Object} additional
      * @param {number} timeout - Default timeout = requestOptions.timeout
      * @returns
      */
      this.saveLog = function (message = null, severity = 1, selfFunction = null, additional = null, timeout = null) {
        log.setCallResTime(getEpochTime() - this.req.startEpochTime);
        log.setRequest({ query: this.req.query, body: this.req.body, params: this.req.params }, this.req.headers, this.req.method, null, this.req.protocol, this.req.url);
        log.setResponse(data, this.statusCode, this.headers);
        log.setCallDirection(1);
        if (typeof selfFunction === 'string') {
          log.setSelfFunction(selfFunction);
        } else {
          log.setSelfFunction(this.req.url);
        }

        if (additional) {
          log.setAdditional(additional);
        }
        if (typeof message === 'string') {
          log.setMessage(message);
        } else {
          log.setMessage(this.statusMessage);
        }

        log.sendIgnoreResponse(severity, timeout || requestOptions.timeout);
      };
      return this;
    };
    next();
  }
}

function getEpochTime() {
  return moment().valueOf();
}