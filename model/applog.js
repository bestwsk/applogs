const request = require('request');
const moment = require('moment-timezone');



const APPLOG_STRUCT = {
    // start_time เวลาเริ่มทำงานของ ฟังก์ชันหรือเซอร์วิส (timezone: UTC) Ex.1514363415
    start_time: 'number', //Required

    // start_time_gmt7 Follow the RFC3339  ex.2017-12-16T19:20:30.45+07:00
    start_time_gmt7: 'number',
    /* ----------- START SELF DATA ------------- */
    // self_type ประเภทของ microservice หรือ application ที่เป็น owner ของ log ตัวนี้
    // ST01 = MobileApp
    // ST02 = Appliance/SmartDevice/IOT
    // ST03 = Platform/Backend/Worker
    // ST04 = Web Front End
    self_type: 'string',//Required

    // ชื่อทีมที่ develop, maintain service ที่เป็น owner ของ log นี้ - DMPSS , DMPCMS , ETC.
    self_group: 'string',//Required

    // ชื่อระบบที่พัฒนา/microservices/ Repository 
    // ex DMPSS => Subscription, Package, System, IndiMsg
    // ex DMPCMS => FNContentDetail, Backoffice
    self_system: 'string',//Required

    // ชื่อโมดูลที่พัฒนาหรือ class name หรือเทียบเท่า
    // เช่น API ที่อยู่ภายใต้ Repo (self_system) นั้นๆ อีกที เช่น ** BNIN => [system: EventRD; module: TYEarn4DMP]
    self_module: 'string',

    // ชื่อฟังก์ชั่นที่พัฒนา
    // เช่น Method ที่อยู่ใน Controller ที่ทำงานภายใต้ API นั้นๆ อาจจะเป็น CRUD function ก็ได้
    // ** BNIN => [system: RBAC; module: -; function: CreateMember]
    self_function: 'string',//Required

    // transaction id ของระบบที่เป็น owner ของ log นี้
    self_tran_id: 'string',


    //TBA
    alliance_tran_id: 'string',

    // ชื่อทีม alliannce ที่ develop, maintain service => TIT_Billing, TIT_MVP, Mobile_TGW, Mobile_ETG
    alliance_group: 'string',

    // ชื่อระบบของ alliance ที่พัฒนา/microservices/Repository ETG, MVP, TGW, CDB, SBM, CBM
    alliance_system: 'string',

    // ชื่อโมดูลที่พัฒนาหรือ class name หรือเทียบเท่า
    alliance_function: 'string',

    // ชื่อฟังก์ชั่นที่เรียกใช้งาน
    alliance_module: 'string',


    /* ----------- END SELF DATA ------------- */

    // 0=self2self, 1 = alliance2self, 2=self2alliance
    call_direction: 'string',//Required 

    //พารามิเตอร์ที่ส่งมาจาก client หรือสิ่งที่ส่งเข้า functionควร log เฉพาะ controller layer (Query param, Post body)
    call_req_params: 'string',//Required 

    //	HTTP Request Headers 
    // "Accept: Application/json, Accept-Charset: utf-8"
    call_req_headers: 'string',//Required

    //  HTTP Verb
    call_req_methods: 'string',//Required

    // HTTP Port
    call_req_port: 'string',

    // HTTP/HTTPS/Your_Desired_Protocol
    call_req_protocol: 'string',
    // HTTP Domain
    call_req_domain: 'string',
    // HTTP Response Body (1000KB)
    call_res_body: 'string',//Required

    // HTTP Response Headers
    call_res_headers: 'string',

    // HTTP Response code
    call_res_status: 'string',//Required

    // ระเวลาที่ทำงาน 
    // call_res_time = start - end time (ms)
    call_res_time: 'number',//Required

    //ระดับความรุนแรงของเหตุการณ์
    //0=Debug 1=Info 2=Warning 3=Error 4=Critical
    call_severity: 'number',//Required

    //message for V2
    message: 'string', //Required

    // for alert 
    web_hook: 'string',

    // System ที่ call เข้ามาที่ API 
    source_system: 'string'
};

// let root = null;
module.exports.ApplogModel = class {
    /**
     * @param {Object} options 
     * @param {string} options.self_system - Your System 
     * @param {string} options.self_group - Your Group 
     * @param {string} options.self_type - Your Type
     * @param {number|string|null} options.start_time - Start Time to Tracking Default = Date().getTime()
     * 
     * @param {Object} requestOptions
     * @param {string} requestOptions.uri - Endpoint URL Default = localhost
     * @param {Object} requestOptions.headers - Headers Paramenter
     * @param {string|null} requestOptions.headers.Authorization
     * @param {number|null} requestOptions.timeout - Default = 5000 ms 
     * @param {boolean} requestOptions.json - Default = true
     * @param {string} requestOptions.method - Method of Request Default = POST
     * 
     * @param {string} mode - normal,loadtest
     * 
     */

    constructor(requestOptions, options, mode = 'normal') {
        this._request = Object.assign({
            uri: 'localhost',
            timeout: 5000,
            json: true,
            headers: {},
            method: 'POST',
            body: {}
        }, requestOptions);
        this._body = Object.assign({
            self_type: '',
            self_group: '',
            self_system: '',
            self_module: null,
            self_function: null,
            self_tran_id: null,
            call_direction: '0',
            call_req_params: {},
            call_req_headers: {},
            call_req_methods: null,
            call_req_port: null,
            call_req_protocol: null,
            call_req_domain: null,
            call_res_body: {},
            call_res_headers: null,
            call_res_status: null,
            call_res_time: 0,
            call_severity: 1,
            alliance_tran_id: null,
            alliance_group: null,
            alliance_system: null,
            alliance_function: null,
            alliance_module: null,
            message: null,
            web_hook: null,
            source_system: null,
            start_time: new Date().getTime()
        }, options);
        this._additional_fields = {};
        this._environment = process.env.NODE_ENV || process.env.DMPENV || 'local';
        this._mode = mode;
        // root = this;
    }

    /**
     *
     *
     
     * @param {string} alliance_group - Alliance Team name
     * @param {string} alliance_system - Alliance System 
     * @param {string} alliance_function - Alliance Function
     * @param {string} alliance_tran_id
     * @param {string} alliance_module
     * @returns
     */
    setAlliance(alliance_group, alliance_system, alliance_function, alliance_tran_id = null, alliance_module = null) {
        Object.assign(this._body, { alliance_tran_id, alliance_group, alliance_system, alliance_function, alliance_module });
        return this;
    }

    /**
     *
     *
     * @param {string} self_function -  This Function name in your Systerm 
     * @returns
     */
    setSelfFunction(self_function) {
        this._body.self_function = self_function;
        return this;
    }

    /**
     *
     *
     * @param {string|null} [self_module=null] - This Module name in your Systerm 
     * @returns
     */

    setSelfModule(self_module = null) {
        if (self_module) {
            this._body.self_module = self_module;
        }

        return this;
    }
    /**
     *
     *
     * @param {string|null} [self_tran_id=null] - Transaction id of your System
     * @returns
     */
    setSelfTransactionID(self_tran_id = null) {
        if (self_tran_id) {
            this._body.self_tran_id = self_tran_id;
        }
        return this;
    }
    /**
     *
     *
     * @param {string} source_system - Ex Facebook App, IG, Twitter
     * @returns
     */
    setSourceSystem(source_system) {
        this._body.source_system = source_system;
        return this;
    }
    /**
     *
     *
     * @param {string} web_hook
     * @returns
     */
    setWebHook(web_hook) {
        this._body.web_hook = web_hook;
        return this;
    }

    /**
     *
     *
     * @param {string} call_direction - 0=self2self, 1 = alliance2self, 2=self2alliance
     * @returns
     */
    setCallDirection(call_direction) {
        this._body.call_direction = call_direction || 0;
        return this;
    }
    /**
     *
     *
     * @param {object} call_req_params - require Data
     * @param {string|object} call_req_headers - HTTP Request Headers
     * @param {string} call_req_methods - GET,POST,PUT,DELETE
     * @param {string|number|null} [call_req_port=null] - HTTP Port Ex. 8080,80,300
     * @param {string|null} [call_req_protocol=null] - HTTP/HTTPS/Your_Desired_Protocol
     * @param {string|null} [call_req_domain=null] - HTTP Domain ,Request Domain
     * @returns
     */
    setRequest(call_req_params, call_req_headers, call_req_methods, call_req_port = null, call_req_protocol = null, call_req_domain = null) {
        Object.assign(this._body, {
            call_req_params: call_req_params,
            call_req_headers: call_req_headers,
            call_req_methods: call_req_methods
        });

        if (call_req_port) {
            this._body.call_req_port = call_req_port;
        }

        if (call_req_protocol) {
            this._body.call_req_protocol = call_req_protocol;
        }

        if (call_req_domain) {
            this._body.call_req_domain = call_req_domain;
        }

        return this;
    }
    /**
     *
     *
     * @param {string|number} call_res_time - elapsed time
     * @returns
     */
    setCallResTime(call_res_time) {
        this._body.call_res_time = call_res_time;
        return this;
    }
    /**
     *
     *
     * @param {string} message - require Parameter for this Log
     * @returns
     */
    setMessage(message) {
        this._body.message = message;
        return this;
    }

    /**
     *
     *
     * @param {object} call_res_body - HTTP Response Body (1000KB)
     * @param {string|number} call_res_status - HTTP Response code Ex 400,200,500
     * @param {string|object} [call_res_headers=null] - HTTP Response Headers
     * @returns
     */
    setResponse(call_res_body, call_res_status, call_res_headers = null) {
        Object.assign(this._body, {
            call_res_body: call_res_body,
            call_res_status: call_res_status
        });

        if (call_res_headers) {
            this._body.call_res_headers = call_res_headers;
        }
        return this;
    }
    /**
     *
     *
     * @param {object} [additional=null] - Every thing your interest
     * @returns
     */
    setAdditional(additional = null) {
        if (additional) {
            this._additional_fields = Object.assign(this._additional_fields, additional);
        }
        return this;
    }
    /**
     *
     *
     * @param {string} environment - Default = process.env.NODE_ENV
     * @returns
     */
    setEnvironment(environment) {
        this._environment = environment;
        return this;
    }

    /**
     *
     *
     * @param {number} [severity=1] - Severity of this log event : 0=Debug , 1=Info , 2=Warning, 3=Error, 4=Critical
     * @returns
     */
    send(severity = 1) {
        this._body.call_severity = severity;
        const log = buildLogBody(this._body);
        log.additional_fields = JSON.stringify(this._additional_fields);
        log.environment = this._environment;
        let _request = Object.assign({}, this._request, { body: log })
        let mode = this._mode;
        return new Promise(function (resolve, reject) {
            if (mode == 'loadtest') {
                return resolve({});
            }
            request(_request, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve({ body, response });
                }
            })
        })
    }
    /**
     *
     *
     * @param {number} [severity=1] - Severity of this log event : 0=Debug , 1=Info , 2=Warning, 3=Error, 4=Critical
     * @param {number} [timeout=500] - time to wait send log/ Default = requestOption.timeout 
     */
    sendIgnoreResponse(severity = 1, timeout = null) {
        if (this._mode != 'loadtest') {
            this._body.call_severity = severity;
            const log = buildLogBody(this._body);
            log.additional_fields = JSON.stringify(this._additional_fields);
            log.environment = this._environment;
            let _request = Object.assign({}, this._request, { body: log, timeout: timeout ? timeout : this._request.timeout });
            request(_request, () => { });
        }

    }
};

function buildLogBody(logInput) {
    let logObj = {};
    for (const key in logInput) {
        if (APPLOG_STRUCT.hasOwnProperty(key)) {
            // const element = logInput[key];
            logObj[key] = validation(logInput[key], APPLOG_STRUCT[key])
        }
    }

    logObj['call_res_time'] = (logObj.call_res_time ? logObj.call_res_time : (new Date().getTime() - logObj.start_time));
    logObj.start_time = Math.round(logObj.start_time / 1000);
    return logObj;
}

function validation(data, type) {
    let result = null;
    switch (type) {
        case 'date':
            if (moment(data).isValid()) {
                result = moment(data).format();
            } else {
                throw new Error(data + ' Not in date type.');
            }
            break;
        case 'string':
            if (typeof data === 'string') {
                result = data;
            } else if (typeof data === 'object' && data != null) { // JSON
                result = JSON.stringify(data);
            } else if (typeof data === 'number') {
                result = data.toString();
            }
            break;
        case 'number':
            if (typeof data === 'number') {
                result = data;
            } else if (typeof data === 'string' && Number.isInteger(data)) {
                result = parseInt(data);
            }
            break;
        default:
            break;
    }
    return result
}
