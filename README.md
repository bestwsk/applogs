#This Beta Test Please Skip 

# Applog V2 Model

## Requirement npm package
* moment-timezone
* request

## Install
```sh
npm i cms_applog_models
```

## How to use
- [Normal](#normal)
- [Middleware](#middleware)


### Normal
applog.js
```javascript
const { ApplogModel } = require('cms_applog_models');
let requestOptions = {
    uri: 'localhost',
    headers: {
        Authorization: 'api_token'
    },
    timeout: 5000,
    json: true,
    method: 'POST'
}
let ApplogConfig = {
    self_system: 'test',
    self_group: 'test',
    self_type: 'test'
}

let mode = 'normal' 

exports.Log = class extends ApplogModel{
    constructor(){
        super(requestOptions,ApplogConfig,mode);
    }
}
```

test.js
```javascript
const { Log } = require('./applog');
let applog = new Log();
applog.setMessage('Test')
    .setRequest({ code: 200, message: 'success' })
    .setEnvironment('local')
    .setSelfFunction('test')
    .send(3);
```

### Middleware

applog.js
```javascript
const { createMiddleware } = require('cms_applog_models');
let requestOptions = {
    uri: 'localhost',
    headers: {
        Authorization: 'api_token'
    },
    timeout: 5000,
    json: true,
    method: 'POST'
}
let ApplogConfig = {
    self_system: 'test',
    self_group: 'test',
    self_type: 'test'
}

let mode = 'loadtest'
exports.logMiddleware = createMiddleware(requestOptions,ApplogConfig,mode);

```

index.js

```javascript
const express = require('express');
const app = express();
const { logMiddleware } = require('./applog');

app.get('/test',logMiddleware,(req,res)=>{
    setTimeout(() => {
        res.send({code:200}).saveLog('Hello')    
    }, 3000);
    
});


app.listen(3000,()=>{
   console.log('Listen on port: ' + 3000);
//    require('./app/worker/consumer_eventRD');
});

```


## Request Options 
see [Request Options](https://www.npmjs.com/package/request#requestoptions-callback)

## ApplogConfig
- `self_system` : system name
- `self_group`  : group of system
- `self_type`   : type of system

## Mode
-  `normal`     : default 
- `loadtest`    : not send log in this mode